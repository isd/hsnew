{-# LANGUAGE NamedFieldPuns #-}
module Main where

import Data.Time
import Options.Applicative
import System.Directory
import System.Process      (callProcess)
import Zhp

licenses =
    [ "GPL-2"
    , "GPL-3"
    , "LGPL-2.1"
    , "LGPL-3"
    , "AGPL-3"
    , "BSD2"
    , "BSD3"
    , "MIT"
    , "ISC"
    , "MPL-2.0"
    , "Apache-2.0"
    , "PublicDomain"
    , "AllRightsReserved"
    ]

categories =
    [ "Codec"
    , "Concurrency"
    , "Control"
    , "Data"
    , "Database"
    , "Development"
    , "Distribution"
    , "Game"
    , "Graphics"
    , "Language"
    , "Math"
    , "Network"
    , "Sound"
    , "System"
    , "Testing"
    , "Text"
    , "Web"
    ]


data Config = Config
    { name    :: String
    , pkgType :: !PkgType
    } deriving(Show, Eq)

data EnvInfo = EnvInfo
    { year :: !Int
    } deriving(Show, Eq)

data PkgType
    = Library
    | Executable
    deriving(Show, Eq, Enum)

boolToPkgType :: Bool -> PkgType
boolToPkgType = toEnum . fromEnum

parser :: Parser Config
parser = Config
    <$> argument str (metavar "NAME")
    <*> parseExe
  where
    parseExe =
        boolToPkgType <$> switch
            ( long "exe"
            <> help "Generate an executable (rather than a library)"
            )

cabalTemplate :: Config -> EnvInfo -> String
cabalTemplate Config{name, pkgType} EnvInfo{year} = unlines $
    [ "cabal-version:       2.2"
    , "name:                " <> name
    , "version:             0.1.0.0"
    , "-- synopsis:"
    , "-- description:"
    , "homepage:            https://git.zenhack.net/zenhack/" <> name
    , "-- license:"
    , concatMap (\l -> "  -- " <> l <> "\n") licenses
    , "license-file:        LICENSE"
    , "author:              Ian Denhardt"
    , "maintainer:          ian@zenhack.net"
    , "copyright:           " <> show year <> " Ian Denhardt"
    , "-- category:"
    , concatMap (\c -> "  -- " <> c <> "\n") categories
    , "build-type:          Simple"
    , "extra-source-files:"
    , "    CHANGELOG.md"
    , "  , README.md"
    , "  , .gitignore"
    , ""
    , "source-repository head"
    , "  type:     git"
    , "  branch:   master"
    , "  location: https://git.zenhack.net/zenhack/" <> name
    , ""
    , "common shared-opts"
    , "  default-extensions:"
    , "      NoImplicitPrelude"
    , "    , OverloadedStrings"
    , "  build-depends:"
    , "      base >=4.14 && <5"
    , "    , zenhack-prelude ^>=0.1"
    , "  default-language:    Haskell2010"
    , ""
    ] <>
    case pkgType of
        Executable ->
            [ "executable " <> name
            , "  import: shared-opts"
            , "  main-is: Main.hs"
            , "  hs-source-dirs:      src"
            ]
        Library ->
            [ "library"
            , "  import: shared-opts"
            , "  hs-source-dirs:      src"
            ]
    <>
    [ "test-suite tests"
    , "  import: shared-opts"
    , "  type: exitcode-stdio-1.0"
    , "  hs-source-dirs: tests"
    , "  main-is: Main.hs"
    ]

gitignore = unlines
    [ "dist"
    , "dist-newstyle"
    , ".ghc.environment.*"
    , "cabal.project.local"
    , ""
    , "# Profiler outputs and formatted reports:"
    , "*.prof"
    , "*.hp"
    , "*.aux"
    , "*.ps"
    , ""
    , ".hspec"
    , ".hspec-failures"
    , ""
    , "# Code coverage:"
    , ".hpc"
    , "*.tix"
    ]

getConfig :: IO Config
getConfig = execParser opts
  where
    opts = info (parser <**> helper)
        ( fullDesc
        <> progDesc "Automatically generate haskell project boilerplate"
        )

getEnvInfo :: IO EnvInfo
getEnvInfo = do
    (year, _, _) <-
        -- Is there really no shorthand for this?
        toGregorian . localDay . zonedTimeToLocalTime <$> getZonedTime
    pure EnvInfo
        { year = fromIntegral year
        }

main :: IO ()
main = do
    cfg <- getConfig
    env <- getEnvInfo
    createDirectory (name cfg)
    setCurrentDirectory (name cfg)
    callProcess "git" ["init"]
    writeFile (name cfg <> ".cabal") $ cabalTemplate cfg env
    writeFile ".gitignore" gitignore
    createDirectory "src"
    when (pkgType cfg == Executable) $
        writeFile "src/Main.hs" $ unlines
            [ "module Main (main) where"
            , ""
            , "import Zhp"
            , ""
            , "main :: IO ()"
            , "main = putStrLn \"Hello, World!\""
            ]
    writeFile "CHANGELOG.md" $ unlines
        [ "# 0.1.0.0"
        , ""
        , "First release."
        ]
    writeFile "README.md" "TODO: write README"
    createDirectory "tests"
    writeFile "tests/Main.hs" $ unlines
        [ "module Main (main) where"
        , ""
        , "import Zhp"
        , ""
        , "main :: IO ()"
        , "main = pure ()"
        ]
