Haskell project boilerplate generator.

`cabal init` asks too many questions; `hsnew` lets me just say:

```
hsnew --exe project
cd project
cabal new-build
```

It also does `git init` and populates a few other things.

This is not appropriate for anyone but myself; in particular it has
some things like hard-coding myself as the author, using my custom
prelude and preferred extensions, etc.
